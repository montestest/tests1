import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://microsites-co-qa.placetopay.ws/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  Inicio/a_Iniciar sesin'))

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  Iniciar sesin/input_Nombre de usuario_email'), 
    'evertec_imesias')

WebUI.setEncryptedText(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  Iniciar sesin/input_Contrasea_password'), 
    '3tK06YFG15u0Ir2bz9iHfI0fCDbsbSWj')

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  Iniciar sesin/button_Iniciar sesin'))

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  Inicio/a_Micrositios'))

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  Micrositios/a_micrositio'))

WebUI.switchToWindowTitle('Micrositios CO | micrositio')

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Micrositios CO  micrositio/a_Ver'))

WebUI.openBrowser('https://microsites-co-qa.placetopay.ws/micropruebas')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/input_Descripcin del pago_description'), 'Pago Micro')

WebUI.selectOptionByValue(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/select_Selecciona una opcin American dollar_91c7ac'), 
    'COP', true)

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/input_Monto_amount'), '1000000')

WebUI.selectOptionByValue(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/select_Selecciona una opcin                _7b4661'), 
    'CC', true)

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/input_Documento del pagador_payer_id'), '1284596215')

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/input_Nombre del pagador_payer_name'), 'Juan')

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/input_Apellido del pagador_payer_surname'), 
    'Cardona')

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/input_Correo electrnico del pagador_payer_email'), 
    'jcardona@gmail.com')

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_micrositio/button_Pagar'))

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/input_land Islands_mobile'), 
    '+57 312 8414952')

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/button_Continuar'))

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/input_Nmero de tarjeta_card_number'))

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/input_Nmero de tarjeta_card_number'), 
    FailureHandling.STOP_ON_FAILURE)

//WebUI.sendKeys(findTestObject('MicrositosWeb/Page_Testing  Checkout Colombia/input_Nmero de tarjeta_card_number'), Keys.chord(Keys.ENTER))
//WebUI.sendKeys(findTestObject('MicrositosWeb/Page_Testing  Checkout Colombia/input_Nmero de tarjeta_card_number'), Keys.chord(Keys.ENTER))
WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/input_Fecha de vencimiento_card_expiration'), 
    '01/31')

WebUI.setEncryptedText(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/input_Cdigo de seguridad (CVV)_card_cvv'), 
    'tzH6RvlfSTg=')

WebUI.selectOptionByValue(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/select_Seleccionar cuotas Diferido con inte_384b6a'), 
    '[object Object]', true)

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/button_Continuar_1'))

WebUI.click(findTestObject('Object Repository/MicrositosWeb/Page_Testing  Checkout Colombia/button_Regresar al Comercio'))

WebUI.closeBrowser()

